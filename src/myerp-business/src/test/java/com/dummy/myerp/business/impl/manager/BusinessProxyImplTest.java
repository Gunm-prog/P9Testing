package com.dummy.myerp.business.impl.manager;

import com.dummy.myerp.business.contrat.manager.ComptabiliteManager;
import com.dummy.myerp.business.impl.BusinessProxyImpl;
import com.dummy.myerp.business.impl.TransactionManager;
import com.dummy.myerp.consumer.dao.contrat.DaoProxy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.mock;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class BusinessProxyImplTest {

    BusinessProxyImpl businessProxyUnderTest;

    @BeforeEach
    @Test
    public void getInstanceTest() {
        DaoProxy mockDaoProxy=mock( DaoProxy.class, Mockito.RETURNS_DEEP_STUBS );
        TransactionManager mockTransactionManager=mock( TransactionManager.class, Mockito.RETURNS_DEEP_STUBS );
        businessProxyUnderTest = BusinessProxyImpl.getInstance( mockDaoProxy, mockTransactionManager );
    }

    @Test
    public void getComptabiliteManagerTest(){
        ComptabiliteManager comptabiliteManager = businessProxyUnderTest.getComptabiliteManager();
        assertThat( comptabiliteManager ).isInstanceOf( ComptabiliteManager.class );
    }

}
