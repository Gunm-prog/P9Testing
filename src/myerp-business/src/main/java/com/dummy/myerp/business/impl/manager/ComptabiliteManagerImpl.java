package com.dummy.myerp.business.impl.manager;

import com.dummy.myerp.business.contrat.manager.ComptabiliteManager;
import com.dummy.myerp.business.impl.AbstractBusinessManager;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.TransactionStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Set;


/**
 * Comptabilite manager implementation.
 */
public class ComptabiliteManagerImpl extends AbstractBusinessManager implements ComptabiliteManager {

    // ==================== Attributs ====================


    // ==================== Constructeurs ====================
    /**
     * Instantiates a new Comptabilite manager.
     */
    public ComptabiliteManagerImpl() {
    }


    // ==================== Getters/Setters ====================
    @Override
    public List<CompteComptable> getListCompteComptable() {
        return getDaoProxy().getComptabiliteDao().getListCompteComptable();
    }


    @Override
    public List<JournalComptable> getListJournalComptable() {
        return getDaoProxy().getComptabiliteDao().getListJournalComptable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<EcritureComptable> getListEcritureComptable() {
        return getDaoProxy().getComptabiliteDao().getListEcritureComptable();
    }

   // @Override
    public EcritureComptable getEcritureComptable(int id) throws NotFoundException {
        return getDaoProxy().getComptabiliteDao().getEcritureComptable(id);
    }

    @Override
    public synchronized void addReference(EcritureComptable pEcritureComptable){
        //init necessary variables
        String journalCode = pEcritureComptable.getJournal().getCode();
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable();

        // 2.  * S'il n'y a aucun enregistrement pour le journal pour l'année concernée :1. Utiliser le numéro 1.
        int numSequence = 1;

        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try{
            //1.  Remonter depuis la persitance la dernière valeur de la séquence du journal pour l'année de l'écriture (table sequence_ecriture_comptable)
            sequenceEcritureComptable = getDaoProxy().getComptabiliteDao().getSequenceEcritureComptable(journalCode, currentYear);

            // * Sinon : 1. Utiliser la dernière valeur + 1
            if (sequenceEcritureComptable != null) {
                numSequence += sequenceEcritureComptable.getDerniereValeur();
            }

        }catch (NotFoundException ignored){ //la sequence n'existe pas il faut la créer
            sequenceEcritureComptable.setAnnee(Calendar.getInstance().get(Calendar.YEAR));
            sequenceEcritureComptable.setDerniereValeur(1);
        }finally {
            //3.  Mettre à jour la référence de l'écriture avec la référence calculée (RG_Compta_5)
            pEcritureComptable.setReference(
                    pEcritureComptable.getJournal().getCode() +
                            "-" + currentYear +
                            "/" + new DecimalFormat("00000").format(numSequence)
            );
        }

        //4.  Enregistrer (insert/update) la valeur de la séquence en persitance (table sequence_ecriture_comptable)
        if(numSequence == 1 ){
            getDaoProxy().getComptabiliteDao().insertSequenceEcritureComptable(sequenceEcritureComptable, journalCode);
        }else{
            getDaoProxy().getComptabiliteDao().updateSequenceEcritureComptable(sequenceEcritureComptable, journalCode);
        }

        getTransactionManager().commitMyERP(vTS);
        getTransactionManager().rollbackMyERP(null);
        }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptableUnit(pEcritureComptable);
        this.checkEcritureComptableContext(pEcritureComptable);
    }


    protected void checkMaxDigitForMontant(BigDecimal montant) throws FunctionalException {
        montant = montant.stripTrailingZeros();
        if( montant.scale() > 2 ){
            throw new FunctionalException("Le montant de la ligne d'écriture comporte plus de 2 decimales.");
        }
    }

    /**
     * Vérifie que l'Ecriture comptable respecte les règles de gestion unitaires,
     * c'est à dire indépendemment du contexte (unicité de la référence, exercie comptable non cloturé...)
     *
     * @param pEcritureComptable -
     * @throws FunctionalException Si l'Ecriture comptable ne respecte pas les règles de gestion
     */
    protected void checkEcritureComptableUnit(EcritureComptable pEcritureComptable) throws FunctionalException {
        // ===== Vérification des contraintes unitaires sur les attributs de l'écriture
        Set<ConstraintViolation<EcritureComptable>> vViolations = getConstraintValidator().validate(pEcritureComptable);
        if (!vViolations.isEmpty()) {
            throw new FunctionalException("L'écriture comptable ne respecte pas les règles de gestion.",
                                          new ConstraintViolationException(
                                              "L'écriture comptable ne respecte pas les contraintes de validation",
                                              vViolations));
        }

        // ===== RG_Compta_2 : Pour qu'une écriture comptable soit valide, elle doit être équilibrée
        if (!pEcritureComptable.isEquilibree()) {
            throw new FunctionalException("L'écriture comptable n'est pas équilibrée.");
        }

        // ===== RG_Compta_3 : une écriture comptable doit avoir au moins 2 lignes d'écriture (1 au débit, 1 au crédit)
        // ===== RG_Compta_7 : le montant des lignes d'écriture peuvent comporter 2 chiffres maximum après la virgule.
        int vNbrCredit = 0;
        int vNbrDebit = 0;
        for (LigneEcritureComptable vLigneEcritureComptable : pEcritureComptable.getListLigneEcriture()) {

            if (BigDecimal.ZERO.compareTo(ObjectUtils.defaultIfNull(vLigneEcritureComptable.getCredit(),
                                                                    BigDecimal.ZERO)) != 0) {
                vNbrCredit++;
                // ===== RG_Compta_7
                checkMaxDigitForMontant(vLigneEcritureComptable.getCredit());
            }
            if (BigDecimal.ZERO.compareTo(ObjectUtils.defaultIfNull(vLigneEcritureComptable.getDebit(),
                                                                    BigDecimal.ZERO)) != 0) {
                vNbrDebit++;
                // ===== RG_Compta_7
                checkMaxDigitForMontant(vLigneEcritureComptable.getDebit());
            }
        }
        // On test le nombre de lignes car si l'écriture à une seule ligne
        //      avec un montant au débit et un montant au crédit ce n'est pas valable
        if (pEcritureComptable.getListLigneEcriture().size() < 2
            || vNbrCredit < 1
            || vNbrDebit < 1) {
            throw new FunctionalException(
                "L'écriture comptable doit avoir au moins deux lignes : une ligne au débit et une ligne au crédit.");
        }

        // vérifier que l'année dans la référence correspond bien à la date de l'écriture, idem pour le code journal...
        // ===== RG_Compta_5 : Format et contenu de la référence
        String[] reference = pEcritureComptable.getReference().split("[-/]");
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int yearInReference = Integer.parseInt(reference[1]);
        String ecritureComptableJournalCode = pEcritureComptable.getJournal().getCode();
        String journalCodeInReference = reference[0];
        String sequence = reference[2];
        if (currentYear != yearInReference) {
            throw new FunctionalException("La référence de l'écriture comptable doit contenir l'année courante.");
        }
        if (!ecritureComptableJournalCode.equalsIgnoreCase(journalCodeInReference)) {
            throw new FunctionalException("La référence de l'écriture comptable doit contenir le code du journal comptable.");
        }
        if (sequence.length() != 5) {
            throw new FunctionalException("La référence de l'écriture comptable doit contenir une séquence de 5 chiffres.");
        }
    }


    /**
     * Vérifie que l'Ecriture comptable respecte les règles de gestion liées au contexte
     * (unicité de la référence, année comptable non cloturé...)
     *
     * @param pEcritureComptable -
     * @throws FunctionalException Si l'Ecriture comptable ne respecte pas les règles de gestion
     */
    protected void checkEcritureComptableContext(EcritureComptable pEcritureComptable) throws FunctionalException {
        // ===== RG_Compta_6 : La référence d'une écriture comptable doit être unique
        if (StringUtils.isNoneEmpty(pEcritureComptable.getReference())) {
            try {
                // Recherche d'une écriture ayant la même référence
                EcritureComptable vECRef = getDaoProxy().getComptabiliteDao().getEcritureComptableByRef(
                    pEcritureComptable.getReference());

                // Si l'écriture à vérifier est une nouvelle écriture (id == null),
                // ou si elle ne correspond pas à l'écriture trouvée (id != idECRef),
                // c'est qu'il y a déjà une autre écriture avec la même référence
                if (pEcritureComptable.getId() == null
                    || !pEcritureComptable.getId().equals(vECRef.getId())) {
                    throw new FunctionalException("Une autre écriture comptable existe déjà avec la même référence.");
                }
            } catch (NotFoundException vEx) {
                // Dans ce cas, c'est bon, ça veut dire qu'on n'a aucune autre écriture avec la même référence.
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insertEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptable(pEcritureComptable);
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().insertEcritureComptable(pEcritureComptable);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptable( pEcritureComptable );// TODO erreur 4 pas de checkEcritureComptable
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().updateEcritureComptable(pEcritureComptable);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteEcritureComptable(Integer pId) {
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().deleteEcritureComptable(pId);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }
}
