package com.dummy.myerp.consumer.dao.impl.db.dao;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertNotNull;


/**
 * Classe de test de l'initialisation du contexte Spring
 */
/*@ExtendWith(SpringExtension.class)*/
@ContextConfiguration(locations = "/com/dummy/myerp/consumer/applicationContext.xml")
public class TestInitSpring extends ConsumerTestCase {

    /**
     * Constructeur.
     */
    public TestInitSpring() {
        super();
    }


    /**
     * Teste l'initialisation du contexte Spring
     */
    @Test
    public void testInit() {
        SpringRegistry.init();
        assertNotNull(SpringRegistry.getDaoProxy());
    }
}
