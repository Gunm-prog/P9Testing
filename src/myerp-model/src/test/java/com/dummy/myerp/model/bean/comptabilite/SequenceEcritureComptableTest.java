package com.dummy.myerp.model.bean.comptabilite;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

public class SequenceEcritureComptableTest {

    @Test
    public void checkGetterSetter(){
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable();

        //Annee
        sequenceEcritureComptable.setAnnee( 2021 );
        assertThat(sequenceEcritureComptable.getAnnee()).isEqualTo( 2021 );

        //DerniereValeur
        sequenceEcritureComptable.setDerniereValeur( 10 );
        assertThat(sequenceEcritureComptable.getDerniereValeur()).isEqualTo( 10 );

        //JournalCode
        sequenceEcritureComptable.setJournalCode( "JC" );
        assertThat(sequenceEcritureComptable.getJournalCode()).isEqualTo( "JC" );
    }

    @Test
    public void shouldReturnStringValueOfSequenceEcritureComptable_whenToString (){
        // GIVEN
        SequenceEcritureComptable sequenceEcritureComptableUnderTest = new SequenceEcritureComptable("BQ", 2021,1);

        // WHEN
        String result = sequenceEcritureComptableUnderTest.toString();

        // THEN
        assertThat(result).isEqualTo("SequenceEcritureComptable{annee=2021, derniereValeur=1}");
    }

}
